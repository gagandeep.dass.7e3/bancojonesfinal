<?php
require_once('../Models/Cliente.php');
require_once('../Helpers/helper.php');
require_once('../Models/ClienteModel.php');
require_once('../Models/Cuenta.php');
require_once('../Models/CuentaModel.php');
require_once('../Models/MovimientoModel.php');
//use Cliente;

if(isset($_POST['submit'])){
    if($_POST['switch_lang'] == 'switch_lang'){
       $lang=$_POST['lang'];
       //setcookie('lang',$lang,time+60*60*24*30,'/','localhost');
       header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    if($_POST['control'] == 'register'){
        $_POST['message']=validarRegister();
        if ($_POST['message']=="Datos correctos"){
            $cliente=new Cliente($_POST['nombre'],$_POST['fechaNacimiento'],$_POST['apellidos'],$_POST['sexo'],$_POST['email'],$_POST['telefono'],$_POST['dni'],$_POST['contrasenya'],"");
            insertCliente($cliente);
            header('Location: ../View/login.php');
        }else{
            require_once('../View/register.php');
        }
    }
    if($_POST['control']=='login'){
        $hash=getUserHash($_POST['dni']);
        if(password_verify($_POST['password'],$hash)){
            session_start();
            $_SESSION['user']=$_POST['dni'];
            echo $_POST['nombre'];
            $_SESSION['nombreUsuario']=$_POST['nombre'];

            header('Location: ../View/welcome.php');
        }else {
            require_once('../View/login.php');
        }
    }
    if($_POST['control'] == 'profile'){
        session_start();
        $check=getimagesize($_FILES['upload']['tmp_name']);
        $fileName=$_FILES['upload']['name'];
        $fileSize=$_FILES['upload']['size'];
        $fileType=$_FILES['upload']['type'];
        echo $fileName . '<br>';
        echo $fileSize . '<br>';
        echo $fileType . '<br>';
        if($check!==false){
            $image=file_get_contents($_FILES['upload']['tmp_name']);
            updateCliente($image,$_SESSION['user']);
        }
        if($_POST['telefono'] != ""){
            updateTelefonoCliente($_POST['telefono'],$_SESSION['user']);
        }
        if($_POST['email'] != ""){
            updateEmailCliente($_POST['email'],$_SESSION['user']);
        }
        if($_POST['contrasenya'] != ""){
            if($_POST['repetirContrasenya'] != ""){
                if($_POST['contrasenya'] == $_POST['repetirContrasenya']){
                    if(contrasenyaCorrecta()){
                        updateContrasenyaCliente($_POST['contrasenya'],$_SESSION['user']);
                    }
                }
            }
        }
        header('Location: ../View/profile.php');
    }
    if($_POST['control']=='create'){
        session_start();
        createAccount($_SESSION['user']);
        header('Location: ../View/cuentas.php');
    }
    if($_POST['control']=='select_account'){
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['saldo']=$saldo;
        header("Location: ../View/query.php");
    }
    if($_POST['control']=='transfer') {
        if (existeCuenta($_POST['cuentas']) && existeCuenta($_POST['cuenta_destino']) && getSaldo($_POST['cuentas'])-$_POST['cantidad']>=0) {
            transfer($_POST['cuentas'], $_POST['cuenta_destino'],$_POST['cantidad']);
            header('Location: ../View/cuentas.php');
        }else{
            header('Location: ../View/transfer.php');
        }
    }
    if($_POST['control']=='query') {
        session_start();
        $_SESSION['saldo']=getSaldo($_POST['cuentas']);
        $_SESSION['lista']=getMovimientos($_POST['cuentas']);
        header("Location: ../View/query.php");
    }
    if($_POST['control']=='select_account'){
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['saldo']=$saldo;
        $_SESSION['lista']=getMovimientos($_POST['cuentas']);
        header("Location: ../View/query.php");

    }
}

?>