<?php

function validarRegister(){
    $validar="";
    $boolean=true;
    if(!nombreCorrecto()){
        $validar=$validar . "*** El nombre solo puede contener letras y espacios ***" . '</br>';
        $boolean=false;
    }
    if(!apellidosCorrectos()){
        $validar=$validar . "*** El apellido solo puede contener letras y espacios ***" . '</br>';
        $boolean=false;
    }
    if(!mayorDeEdad()){
        $validar=$validar . "*** Tienes que ser mayor de edad ***" . '</br>';
        $boolean=false;
    }
    if(!sexoCorrecto()){
        $validar=$validar . "*** Elige un sexo ***" . '</br>';
        $boolean=false;
    }
    if(!dniCorrecto()){
        $validar=$validar . "*** Introduce un dni correcto ***" . '</br>';
        $boolean=false;
    }
    if(!telefonoCorrecto()){
        $validar=$validar . "*** Telefono movil incorrecto ***" . '</br>';
        $boolean=false;
    }
    if(!emailCorrecto()){
        $validar=$validar . "*** El email es incorrecto ***" . '</br>';
        $boolean=false;
    }
    if(!contrasenyaCorrecta()){
        $validar=$validar . "*** La contraseña debe contener mayúsculas, minúsculas, números, un carácter especial y más de 8 caracteres ***" . '</br>';
        $boolean=false;
    }
    if($boolean){
        return "Datos correctos";
    }
    return $validar;
}
function dniCorrecto(){
    if($_POST['dni']){
        $letra = substr($_POST['dni'], -1);
        $numeros = substr($_POST['dni'], 0, -1);
        if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}
function nombreCorrecto(){
    if($_POST['nombre']){
        if (ctype_alpha(str_replace(' ', '', $_POST['nombre'])) === false)  {
            return false;
        }else{
            return true;
        }
    }else{
        return false;
    }
}

function apellidosCorrectos(){
    if($_POST['apellidos']){
        if (ctype_alpha(str_replace(' ', '', $_POST['apellidos'])) === false)  {
            return false;
        }else{
            return true;
        }
    }else{
        return false;
    }
}

function sexoCorrecto(){
    if($_POST['sexo']){
        if($_POST['sexo'] == "Elige una opción"){
            return false;
        }else{
            if($_POST['sexo']== "Hombre"){
                $_POST['sexo']="H";
            }else{
                $_POST['sexo']="M";
            }
            return true;
        }
    }else{
        return false;
    }
}

function mayorDeEdad(){
    if($_POST['fechaNacimiento']){
        $date_parts1=explode("-", $_POST['fechaNacimiento']);
        $date_parts2=explode("-", $today = date("Y-n-j"));
        if($date_parts2[0] - $date_parts1[0] > 18){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }


}

function telefonoCorrecto(){
    if($_POST['telefono']){
        if(strlen($_POST['telefono']) == 9 ){
            if($_POST['telefono'][0] == 6 || $_POST['telefono'][0] == 7){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }else{
        return false;
    }

}

function emailCorrecto(){
    if($_POST['email']){
        return (false !== filter_var($_POST['email'], FILTER_VALIDATE_EMAIL));
    }else{
        return false;
    }
}

function contrasenyaCorrecta(){
    if($_POST['contrasenya']){
        if(!strlen($_POST['contrasenya']) < 8){
            if (preg_match('`[a-z]`',$_POST['contrasenya'])){
                if (preg_match('`[A-Z]`',$_POST['contrasenya'])){
                    if (preg_match('`[0-9]`',$_POST['contrasenya'])){
                        if(preg_match('/[^a-z0-9 _]+/i', $_POST['contrasenya'])) {
                            if($_POST['repetirContrasenya']==$_POST['contrasenya']){
                                return true;
                            }else{return false;}
                        }else{return false;}
                    }else{return false;}
                }else{return false;}
            }else{return false;}
        }else{return false;}
    }else{return false;}
}

?>