<?php
class Cliente{
    private $nombre;
    private $nacimiento;
    private $apellidos;
    private $sexo;
    private $email;
    private $telefono;
    private $dni;
    private $password;
    private $image;


    public function __construct($nombre, $nacimiento, $apellidos, $sexo, $email, $telefono, $dni, $password, $image)
    {
        $this->nombre = $nombre;
        $this->nacimiento = $nacimiento;
        $this->apellidos = $apellidos;
        $this->sexo = $sexo;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->dni = $dni;
        $this->password = $password;
        $this->image = $image;
    }



    public function getImage()
    {
        return $this->image;
    }


    public function setImage($image)
    {
        $this->image = $image;
    }


    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    public function setNacimiento($nacimiento)
    {
        $this->nacimiento = $nacimiento;
    }


    public function getApellidos()
    {
        return $this->apellidos;
    }


    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    public function getSexo()
    {
        return $this->sexo;
    }


    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function setEmail($email)
    {
        $this->email = $email;
    }


    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    public function getDni()
    {
        return $this->dni;
    }

    public function setDni($dni)
    {
        $this->dni = $dni;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }



}
?>