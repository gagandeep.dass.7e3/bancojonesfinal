<?php

require_once ("../Models/DBAManager.php");
require_once ("../Models/Cliente.php");
//use DBManager;
//use Cliente;

function insertCliente($cliente){
    $manager=new DBManager();
    try{
        $sql="INSERT INTO cliente (nombre,nacimiento,apellidos,sexo,email,telefono,dni,password) VALUES (:nombre,:nacimiento,:apellidos,:sexo,:email,:telefono,:dni,:password)";
        $password=password_hash($cliente->getPassword(),PASSWORD_DEFAULT,['cost'=>10]);
        $stmt= $manager->getConnection()->prepare($sql);
        $stmt->bindParam(":nombre",$cliente->getNombre());
        $stmt->bindParam(":nacimiento",$cliente->getNacimiento());
        $stmt->bindParam(":apellidos",$cliente->getApellidos());
        $stmt->bindParam(":sexo",$cliente->getSexo());
        $stmt->bindParam(":email",$cliente->getEmail());
        $stmt->bindParam(":telefono",$cliente->getTelefono());
        $stmt->bindParam(":dni",$cliente->getDni());
        $stmt->bindParam(":password",$password);

        if($stmt->execute()){
            echo "TODO BIEN";
        }else{
            echo "MAL";
        }
    } catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateTelefonoCliente($telefono, $dni){
    $conexion=new DBManager();
    try{
        $sql="UPDATE cliente SET telefono=:telefono WHERE dni=:dni";
        $stmt=$conexion->getConnection()->prepare($sql);
        $stmt->bindParam(':telefono',$telefono);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateEmailCliente($email, $dni){
    $conexion=new DBManager();
    try{
        $sql="UPDATE cliente SET email=:email WHERE dni=:dni";
        $stmt=$conexion->getConnection()->prepare($sql);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateContrasenyaCliente($contrasenya, $dni){
    $password=password_hash($contrasenya,PASSWORD_DEFAULT,['cost'=>10]);
    $conexion=new DBManager();
    try{
        $sql="UPDATE cliente SET password=:password WHERE dni=:dni";
        $stmt=$conexion->getConnection()->prepare($sql);
        $stmt->bindParam(':password',$password);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateCliente($image,$dni){
    $conexion=new DBManager();
    try{
        $sql="UPDATE cliente SET imagen=:img WHERE dni=:dni";
        $stmt=$conexion->getConnection()->prepare($sql);
        $stmt->bindParam(':img',$image,PDO::PARAM_LOB);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}


function getActualUser($dni){
    $conexion=new DBManager();
    try{
        $sql='SELECT * FROM cliente WHERE dni=:dni';
        $stmt=$conexion->getConnection()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function getUserHash($dni){
    $conexion=new DBManager();
    try{
        $sql='SELECT * FROM cliente WHERE dni=:dni';
        $stmt=$conexion->getConnection()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['password'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}



?>
