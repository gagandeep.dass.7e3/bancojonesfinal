<?php
class Cuenta{
    private $id;
    private $id_cliente;
    private $creacion;
    private $saldo;

    /**
     * Cuenta constructor.
     * @param $id
     * @param $id_cliente
     * @param $creacion
     * @param $saldo
     */
    public function __construct($id, $id_cliente, $saldo)
    {
        $this->id = $id;
        $this->id_cliente = $id_cliente;
        $this->saldo = $saldo;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * @param mixed $id_cliente
     */
    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    /**
     * @return mixed
     */
    public function getCreacion()
    {
        return $this->creacion;
    }

    /**
     * @param mixed $creacion
     */
    public function setCreacion($creacion)
    {
        $this->creacion = $creacion;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }


}
?>