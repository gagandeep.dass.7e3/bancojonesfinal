<?php
require_once('../Config/config.php');

class DBManager extends PDO
{
    public $server = SERVER;
    public $port = PORT;
    public $db = DB;
    public $user = USER;
    public $pass = PASS;

    private $connection;

    public function __construct(){
        $this -> conectar();
    }

    private final function conectar(){
        $connection = null;

        try {
            if (is_array(PDO::getAvailableDrivers())){
                if (in_array("pgsql",PDO::getAvailableDrivers())){
                    $connection = new PDO("pgsql:host=$this->server;port=$this->port;dbname=$this->db;user=$this->user;password=$this->pass");
                }else{
                    throw new PDOException("Error");
                }
            }
        }catch (PDOException $e){
            echo $e->getMessage();
        }
        $this->setConnection($connection);
    }

    public final function getConnection(){
        return $this->connection;
    }

    public final function setConnection($connection){
        $this->connection = $connection;
    }

    public final function closeConnection(){
        $this->setConnection(null);
    }


}
