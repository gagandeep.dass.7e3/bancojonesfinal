<?php

require_once ("../Models/DBAManager.php");

function transfer($origen, $destino, $cantidad){
    $manager = new DBManager();
    try {
        $lastId=getLastIdMovimiento()+1;
        $sql = "INSERT INTO movimientos (id,fecha,cantidad,id_origen,id_destino) VALUES (:id,now(),:cantidad,:id_origen,:id_destino)";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':id', $lastId);;
        $stmt->bindParam(':cantidad', $cantidad);
        $stmt->bindParam(':id_origen', $origen);
        $stmt->bindParam(':id_destino', $destino);
        $stmt->execute();
        $sql = "UPDATE cuenta SET saldo = saldo - $cantidad WHERE id=:origen";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':origen', $origen);
        $stmt->execute();

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
function getMovimientos($cuenta)
{
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM movimientos WHERE id_origen=:cuenta OR id_destino=:cuenta";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':cuenta', $cuenta);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //$manager->cerrarConexion();
        return $rt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function getLastIdMovimiento(){
    $manager = new DBManager();
    try {
        $sql = "SELECT id FROM movimientos ORDER BY id DESC limit 1";
        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //$manager->cerrarConexion();
        if (sizeof($rt)>0){
            return $rt[0]['id'];
        }else{
            return 0;
        }
    }catch(PDOException $e){
        echo $e->getMessage();
    }
}

?>
