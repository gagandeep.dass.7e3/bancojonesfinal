<?php
session_start();
if(isset($_SESSION['user'])){

}else{
    header('Location: ../View/login.php');
}

?>

<html>
<head>
    <title>Cuentas</title>
    <link rel="stylesheet" href="../estilos.css">
</head>
<body>
    <?php require_once('../View/header.php'); ?>
    <h1>Cuentas</h1>
    <nav>
        <a href="welcome.php">Atras</a>
        <a href="profile.php">Profile</a>
        <a href="transfer.php">Transferencia</a>
        <a href="logout.php">Logout</a>
    </nav>
    <form action="../Controller/controller.php" method="post" class="formualario-cuentas">
    <p styles="padding:10px;">Cuentas del usuario:</p>
        <select name="cuentas" class="cuentas-select">

            <?php
            require_once('../Models/CuentaModel.php');
            session_start();
            $accounts=getAccounts($_SESSION['user']);
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["id"] ?></option>
            <?php }?>
        </select>
        <input name="submit" type="submit" value="Seleccionar"/>
        <input name="control" type="hidden" value="select_account"/>
    </form>
    <form action="../Controller/controller.php" method="post" clas="formulario-operacion">
        <input name="submit" type="submit" value="Crear cuenta"/>
        <input name="control" type="hidden" value="create"/>
    </form>
</body>
</html>

<?php

?>
