<html>
<head>
    <link rel="stylesheet" href="../estilos.css">
    <title>Banco Jones</title>
</head>
<body>
<?php require_once('../View/header.php'); ?>
<h2>Tu sesión se ha cerrado.
    <span>Gracias por usar Banco Jones</span>
</h2>
<div class="user-actions">
    <a href="register.php">REGISTRAR</a>
    <a href="login.php">INICIAR SESSIÓN</a>
</div>

</body>
</html>

<?php
    session_start();
    session_unset();
    session_destroy();
?>
