<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../estilos.css">
    <title>Perfil</title>
</head>

<body>
<?php require_once('../View/header.php'); ?>
<h1>Perfil</h1>
<nav>
    <a href="profile.php">Perfil</a>
    <a href="transfer.php">Transferencia</a>
    <a href="cuentas.php">Cuentas</a>
    <a href="upload.php">Actualizar/Subir Foto</a>
    <a href="logout.php">Cerrar sessión</a>
</nav>

<form action="../Controller/controller.php" method="post" class="formulario-perfil">

    <div>
        <label for="name">Nombre:</label>
        <input name="name" type="text">
    </div>

    <div>
        <label for="apellido">Apellidos:</label>
        <input name="surname" type="text">
    </div>

    <div>
        <label for="genero">Selecciona tu género:</label>
        <select>
            <option>Hombre</option>
            <option>Mujer</option>
        </select>
    </div>

    <div>
        <label for="birthdate">Fecha de nacimiento:</label>
        <input name="birthdate" type="date" >
    </div>

    <div>
        <label for="mobilePhone">Numero de teléfono:</label>
        <input name="mobilePhone" type="tel">
    </div>

    <div>
        <label for="email">Email:</label>
        <input name="email" type="email">
    </div>

    <div>
        <label for="password">Contraseña:</label>
        <input name="pass" type="password" >
    </div>

    <div>
        <label for="repeatPassword">Contraseña:</label>
        <input name="repeatPass" type="password">
    </div>

    <div>
        <input type="hidden" value="profile" name="control">
    </div>
    <div class="save-button">
        <input type="submit" name="submit" value="Actualizar Datos">
    </div>
</form>
</body>

</html>
