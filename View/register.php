<?php 
require_once('../Helpers/i18n.php');
?>
<html>
<head>
    
    <title><?php echo _("Registro"); ?></title>
</head>
<body>
<?php require_once('../View/header.php'); ?>
    <form name="register" action="../Controller/controller.php" method="post">
        <!--<p><?php echo _("Registro"); ?>:</p>-->
        <p>Nombre:</p>
        <input name="nombre" type="text" value="<?php if (isset($_POST['nombre'])) echo $_POST['nombre']?>"/>
        <p>Apellidos:</p>
        <input name="apellidos" type="text" value="<?php if (isset($_POST['apellidos'])) echo $_POST['apellidos']?>"/>
        <p>Sexo:</p>
        <select name="sexo">
            <option selected value="0"> Elige una opción </option>
            <option value="1">Hombre</option>
            <option value="2">Mujer</option>
        </select>
        <p>Fecha de nacimiento:</p>
        <input name="fechaNacimiento" type="date" value="<?php if (isset($_POST['fechaNacimiento'])) echo $_POST['fechaNacimiento']?>">
        <p>DNI:</p>
        <input name="dni" type="text" value="<?php if (isset($_POST['dni'])) echo $_POST['dni']?>"/>
        <p>Telefono:</p>
        <input name="telefono" type="tel" value="<?php if (isset($_POST['telefono'])) echo $_POST['telefono']?>">
        <p>Email:</p>
        <input name="email" type="email" value="<?php if (isset($_POST['email'])) echo $_POST['email']?>">
        <p>Contraseña:</p>
        <input name="contrasenya" type="password" >
        <p>Repite contraseña:</p>
        <input name="repetirContrasenya" type="password" >
        <input name="control" value="register" type="hidden"/>
        <br><br>
        <input name="submit" value="Registrar" type="submit"/>
    </form>
    
    <?php
    if(isset($_POST['message'])){
        echo $_POST['message'] . '</br>';
    }
    require_once('../Helpers/i18n.php');
    ?>

</body>
</html>

