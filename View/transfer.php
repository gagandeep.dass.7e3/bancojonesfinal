<?php
session_start();
if(isset($_SESSION['user'])){

}else{
    header('Location: ../View/login.php');
}

?>

<html>
<head>
    <link rel="stylesheet" href="../estilos.css">
    <title>Transfer</title>
</head>
<body>
<?php require_once('../View/header.php'); ?>
<h1>Transferencias</h1>
<nav>
    <a href="welcome.php">Atras</a>
    <a href="profile.php">Perfil</a>
    <a href="cuentas.php">Cuentas</a>
    <a href="query.php">Query</a>
    <a href="logout.php">Cerrar sessión</a>
</nav>

<form action="../Controller/controller.php" method="post">
    </br></br>
    <select name="cuentas">
        <?php
        require_once('../Models/CuentaModel.php');
        session_start();
        $accounts=getAccounts($_SESSION['user']);
        for ($i=0; $i<sizeof($accounts) ;$i++){?>
            <option ><?php echo $accounts[$i]["id"] ?></option>
        <?php }?>
    </select></br></br>
    Introduce la cuenta de destino: </br></br><input name="cuenta_destino" type="text" /></br></br>
    Introduce la Cantidad: </br></br><input name="cantidad" type="text" /></br></br>
    <input name="submit" type="submit" value="Seleccionar"/>
    <input name="control" type="hidden" value="transfer"/>
</form>
</body>
</html>
