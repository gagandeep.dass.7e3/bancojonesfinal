--
-- PostgreSQL database dump
--

-- Dumped from database version 11.9 (Debian 11.9-0+deb10u1)
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cliente; Type: TABLE; Schema: public; Owner: itb
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    nombre text,
    nacimiento date NOT NULL,
    apellidos character varying(200),
    sexo character(1),
    email character varying(200) NOT NULL,
    telefono character varying(9) NOT NULL,
    dni character varying(9) NOT NULL,
    password character varying(200) NOT NULL,
    image bytea
);


ALTER TABLE public.cliente OWNER TO itb;

--
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: itb
--

CREATE SEQUENCE public.cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_seq OWNER TO itb;

--
-- Name: cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itb
--

ALTER SEQUENCE public.cliente_id_seq OWNED BY public.cliente.id;


--
-- Name: cuenta; Type: TABLE; Schema: public; Owner: itb
--

CREATE TABLE public.cuenta (
    id integer NOT NULL,
    id_cliente integer NOT NULL,
    saldo numeric(10,2)
);


ALTER TABLE public.cuenta OWNER TO itb;

--
-- Name: cuenta_id_seq; Type: SEQUENCE; Schema: public; Owner: itb
--

CREATE SEQUENCE public.cuenta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuenta_id_seq OWNER TO itb;

--
-- Name: cuenta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itb
--

ALTER SEQUENCE public.cuenta_id_seq OWNED BY public.cuenta.id;


--
-- Name: example; Type: TABLE; Schema: public; Owner: itb
--

CREATE TABLE public.example (
    id integer NOT NULL,
    nombre text
);


ALTER TABLE public.example OWNER TO itb;

--
-- Name: example_id_seq; Type: SEQUENCE; Schema: public; Owner: itb
--

CREATE SEQUENCE public.example_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.example_id_seq OWNER TO itb;

--
-- Name: example_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itb
--

ALTER SEQUENCE public.example_id_seq OWNED BY public.example.id;


--
-- Name: movimientos; Type: TABLE; Schema: public; Owner: itb
--

CREATE TABLE public.movimientos (
    id integer NOT NULL,
    fecha timestamp without time zone,
    cantidad numeric(100,2),
    id_origen integer,
    id_destino integer
);


ALTER TABLE public.movimientos OWNER TO itb;

--
-- Name: movimientos_id_seq; Type: SEQUENCE; Schema: public; Owner: itb
--

CREATE SEQUENCE public.movimientos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimientos_id_seq OWNER TO itb;

--
-- Name: movimientos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: itb
--

ALTER SEQUENCE public.movimientos_id_seq OWNED BY public.movimientos.id;


--
-- Name: cliente id; Type: DEFAULT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cliente ALTER COLUMN id SET DEFAULT nextval('public.cliente_id_seq'::regclass);


--
-- Name: cuenta id; Type: DEFAULT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cuenta ALTER COLUMN id SET DEFAULT nextval('public.cuenta_id_seq'::regclass);


--
-- Name: example id; Type: DEFAULT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.example ALTER COLUMN id SET DEFAULT nextval('public.example_id_seq'::regclass);


--
-- Name: movimientos id; Type: DEFAULT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.movimientos ALTER COLUMN id SET DEFAULT nextval('public.movimientos_id_seq'::regclass);


--
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: itb
--

COPY public.cliente (id, nombre, nacimiento, apellidos, sexo, email, telefono, dni, password, image) FROM stdin;
8 a 1983-08-08 as h a@gmail.com	89999992 00000000E 12345 \N
7 victor 1983-08-08 marquina h v@gmail.com	666666666 00000000Z $2y$10$YfU5z9zKudsYtWwMdpxvm.9eqFUSiWVGVtTggW3krmClmxDSlU6ia \\xffd8ffe000104a46494600010100000100010000ffdb00840009060710131015121212151515161612131612181517121716171518171816161515181d2820181a271b171521312125292d2e3a2e17203f38332e37282d2e2b010a0a0a0e0d0e1b10101b37251f2637372f32342e302d372b312b2b372f372b2f2d302d2d2e2d2b2b2b2d2f2b2d2d2d2b2d2d2b2d2f2b2b2d2d2d2d2d2d2d2d2fffc000110800f400ce03012200021101031101ffc4001c0001000105010100000000000000000000000703040506080201ffc4004410000103020207050408020905010000000100020304110521060712314151611322718191324252a10814728292b1c1d123a2244344536273b2c2e13363d2f0f115ffc4001b01010002030101000000000000000000000001050204060307ffc4002c110100020201030302040700000000000000010203110405123121416113712223d1f12432515281e1f0ffda000c03010002110311003f009c51110111101111011110111101111011110111101111011110111101111011110111101111011110111101116174c748a3a0a296aa4cf6059ace2f7bb2634789f903c9059e9b69bd1e191074ee2e7bafd9c0dcdefb71ff000b7a9506e916ba7149c91016d2b3930091f6eb23c7cc00b45c77189eaea1f5150fdb91e6e4f003835a38340c80560833551a5b893ced3ab6a49ff39e3f22aee835818bc26f1d74de0e776a3cdaf0415ad2facde1489af44f5ecf05acc42105bbbeb110b11d5d16e3e447829ab0ac4e0a989b353c8d92377b2f69b836de3a11c41cd72e1c0e37d3078c8d978d04d31a8c26aae2ef81c409a1be4e1f136fb9e381f22a0758a2b6c36be29e164d1383a391a1ed70e20feaae5011110111101111011110111101111011110111101411f490c689929a89a720d350f1ccb89647e803fd429dd730ebe66dac6641f0c50b7e57fd504788ae22a390b76fb3796717869238f1b5b81f45e278b648b38381170e1f911c0f452292fac36217c59bc23452b2a06d471d9a7739c766fe17ccf8a0a13e352160634d8058e9652edeb3188e8fcb01d89e374325aecdaef3241fe178caeb088270fa3ae931bcb873c920033c37e19812307990eb7da538ae46d5862060c628de3de99911ced94c7b337e836afe4bae54022220222202222022220222202222022220222202e6dd656186a7498d3804f68606d871fe18245f86edeba49407a55388b4c607bb717d30e5edb3607cca0db9bffe3d3cada0a8aa0d9c0602d6348647b7ecb5d21616b6f71bc8de39a8c75cba14cc3ea2392124c33871b1b0d991a7bc3216b105a7d563b58534d4f8d566ddfbf23aed3b9f1c8d05b96e22c42deb4ba9aaf12c2b0c86366d4ad8e39a599ee0d6b6f1ec869be6e71f68d81dc2fbd79e4cb4c51dd7b4447ca6b59b788479ab0d1a6d7e22c81e6cc6b5f2bf2bdc36c00b1dfde737caea69d21afc1f0b7b2195d3cb33c02228f65ef03e270eeb40e9bf905abeaef47abf0fc4239a68e1918e6ba27be378db631d63b45a402e01cd6ee072bad235c1da371ca973ef7da89cc37f77b366c1691c32e1c9462cf8f2fae3b44fd936acd7cc264c670ca4c5b0c7fd55c2402f60e1b32c32b45ec41176b85c5c71078dd733baf7cf7f153b6a03119e67d64b27b223a76b9c05839eddbef1e1b5b36bf80508e252b5f34af6fb2e92470e1917123e4bd58ae7469f6ada63caa203e9235767ae2cc04ff4b83fce87fd6d5da6808888088880888808888088880888808888088880b9ab5f0f7331b0f6121c22a77b48de1cd2eb11d6e174aa8035e0238f1ca5964f63b08cb8dafecc92dbfdab1b4ea265311b9d3669e68710652cf5348d6d446d04b9d9907e1b0c8b6f980edc4f0599a7601b85ae6e7a9c85fe4160704aa648c0e638386472ebb96c10ae079f9f266cb3393f65d531d694d55782958e2d796b4b9becbad98f02b1b8f68dd1d6002a216bcb7d975cb5c0720e6906dd16641cb2587acc61becc5671e2fdec6f9fbc7a0f35e5bbd66271ccc4c7bc3ce2bdde92a38ad13a0c324a4c3238a0739a4665c2fb42cf76d1b932168b024f2e4b9a082323911959740d4ce2263e424936739ce26e4d813e9d37280d8d7cb25866e7b89e599cd75bd23939b356df5277ad7ab4f958ab8f5a7da19b62563cfbaf63bd082bb5e17ed343b9807d45d71557d04b0bb6646969f97aaeb0d596302ab0aa596f7708db13cf1db8bb8e27a9b5fcd5c351b422220222202222022220222202222022220222202e7bfa48cad35b4cd07bcd8093d039e767f22a73d21c6e0a3a67d4ceed96305fab8f063471713900b989ff005ac7b18dd632bbc44303399e8df571ea8242a1c1dc2829258eed9053c37b1b13dc17f355e8f147119cf203cb662f4bec290710c28318d6c63bac6b5a07468007e4b58abc16079bb9963cc65f92aae774daf227babe92dac3c8ec8edb7862e5a863877dee78e4e7123f0eef92b9a589cff6465cce43c95fd1601103dd65cf5cff0035b461f83dac5de8b5f0745ac4ef2cefe3d9e97e67a6a91a60e9303f7b6768f5fd973d69ae08fa0c4248802d01c2488f363b36dbc336fdd2baf191802c028b35fba2ddbd1b6b231fc4a6bed803da85c46d5fec9cfc0b95dd2b158d56350d299999dca34abc6e9a7a126423b40db58efdab705b0fd1e74a0453c94121b367fe2457fef5a3bcdf17340fc1d543eab52553e291b246e2c7b1c1cd78c8870cc10b343b6d16b1abcd2d8f12a264e2c246f7268fe0906ffba778e87a2d9d402222022220222202222022220222202b7afad8e189f2caf0c8d8d2e73ce4001bc9570b9d35e3a766a6734103bf810bad29045a59470ead61cbc6fc820d7359da77262751ddbb69a3244311e3c0c8f1f11f90f3bcd9a9cd0d143442591bfd22a035ef2466c6117645d2dbcf53d173f68150367c4e96277b265639c39b63efb8798691e6bafd8f045c116491f5ed0458ac74d84309bac95d5232e6a053a6a36b370572975e1d3346f283daa55703248dcc78bb1ed735c0f16b8588f454dd5d18f795bcd5ed76410721691e14ea5ab9e99dbe291ec079b41eebbcdb63e6b1ca49d7d61c63c4db3069d99e18ddb56c8bd976385f89b3584fda0a36590d934074ba6c36adb3b2ee8cd9b3457b0919ff0090de0fee5757e098bc157032a20787c6f170788e6d70e0e072217162deb551a76fc3aa83247134b2b80959c184d8095a398cafcc780503a9d1798de1c0381041008233041dc415e9011110111101111011110111106b3ac8c78d1619513b4d9fb1d9c67fee49dd691e17bf92e4a8a22eda7389b0cdcede493c3a9254e1f48fc586c52d134f79ce350e1d00746cb8ea4c9f84a87aa69dce923a485a5ceda6b6c3df95f61f2bdbd506eba90d1796a2b3eb4328e0b8bfc4f7b48d9f00d7127c974653d1b5a399ff00de0b1fa1da3d1d051454b1fb82ef77c723b37b8f89f959665402a7d9f7b6ba01f9ff00c2a8880be1685f510509692376f685613e136cd87c8acb220d3b49347e2afa57d24e2c4ddd1496b98e403bae1d3811c412b986be8e4a59e4a79d9de8dee63dbd47169e47783c9764cf4e1d9f1e6a0dfa41e8e6ccf4f5ad1ff547612fdb60bb1de6db8fba14c088e6a41b3b719da6f11ef37ed0568b3d24021b4833cace6fc4d3bc2c6e294c18feee6c700f69e8730a47416a0f4a8d4513a92437929764349f7a175f63f091b3e1b2a535c95aacc7bea78ac1217598f77632722c93bb9f83b65df7575aa80444404444044440444405e5ee00124d800493c80de57a51a6bd34afeab41f568cda6aa0e665eec42dda1f30437ef1e48219d32d21faee275159be3612d8b88d961d98bd4f7add4acf6a0f02fac624ea97e6da66f69e3249b4d65ff9dde2d0a3c06d4c7aca2ff75bff002ba1fe8fd8588b0b335bbd3caf75ed9ecb3b8d1e170e3e65049a888a07990e45233905e67f64af51ee083d22220222202d235cf87f6d8354102ee8bb39dbd361e368fe02f5bbac2e9b41b786d5b0f1a7987f2141c7f254388b12afaa8ed52c4ee2d73a3f2de3e45631652716a38c7173deefd3f4590c5aec4d06c5feb78753541372f89bb477f7dbdd7f9ed02b8ed7487d1e6bcbf0b7c47fa99ded1f65e1affccbbd54094511101111011110111101724eb431d75662b512127658f30c63932225b9789da77de5d6cb98eb356d552d7d58739b131b3cbb2e2368b8171734802d9588cd7966cf8f0d7bf24ea195296bceab0d4aa6836689b25c1bcb9819daed36bfa2ea1d59d288f07a268e3046ff003907687e6e2b9fb16d0fa8a26384ae1253bc58c8d07f86ef75ce69dc2fc5744e81d4c6fc3697b37b5db10c51bb64dece6300734f237098b363cd5efc73b82f4b5275686791117a31539fd95edbb953a8dcaa37720fa8888088880b11a61206e1f54e3c209bfd0565d6afad0a911e0f5ae277c2e679c9660f9b820e480164b1a758b22feed8d69fb5ef7ceeade95f1b2ce702e70b102f603913ccf4559f5103dc4bdae049b97037fcd6431ea77fa35487b2ac6f00f84f996b81fc828965c003a2ed60936c0ccb4e454d9f474a12cc3a694ff5b3903c236b47e65de8a04b0888808888088880be170e6b01a635f2471359192d748e2368644002e6c781dcb53650022e49279dc9551ceead4e2dfb3b772dbc3c59c95eede9bc6358d454f192482ff7597cc9fd0755a651971bb9c6ee71249e64ef54db8736f7578c6d82e73a8f52b72f51ad447b37f0608c51f2a755035ec731c016b81041cc107782a246e255980e205d012ea790dfb271ee48d1ee139ecbdb7c9c33ddbc1214c2b5dd34d1f6d5d33a3dce1de63b9387e9c0f4ba74ae74f1b2ea7f967cfebfe11c9c3f52bf309074474aa9710804d4efbee0f8ce4f8ddf0bdbfaee3c167171b60d8bd5e1f55da42f7452c66ce6f0758e6c7b7739a792e91d5d6b1e9b126f666d154b45dd09393ec337447de6f4de3e6bb9f985337599b70bd3372fa8808888088a8d5c9b2d2506bfa7356f647135a6c1f259dd4069c8f4fd960aa246885c5cd6bbba4ec900824662e3c405574aea1cf6c57e127fb4aa32b6f1ae4bad46f93ebf0b5e2444e3732137cd16dfa43a0f511cae30376e326ed682039b7e041e0159e1da135d2bac63118f89e45bd05cae96bccc134efee8d2be706489d696fa3ed9667b696069db90d8bb90f79c7a0172ba7741443042ca289bb2d8dae2dcee4ddd771279ddd7f35a368568ac344c3b3de91c3bd29199e8de4de8b6dc1cecd5464f1bb7f10cbe7654d6ead393974ad27f078fbeff00ef46dc717b714cdbcb754445d12bc4444044441ac69cb7b913b93c8f569fd82c3c67259ad3d3fd1d9cfb46dbd1cb054fec85c675eaeb93bf885bf0e7f29551115236c5f085f510447ad9d1bd870ab8dbdd759b201c0f077e9e8a3da2ab92291b2c4f2c7b08735ed362d23710574a62546c9a27c4f176bc1691e2a33c335355350f7767530b636bcb5db5b66468be5760163975b1b2ec3a273beae3fa379f5af8f98ff4aae5e1edb7747894e3a03a406bf0e82a9c2cf7b5c1e06edb638b1c47425b71d0ad8162b45b028a868e2a48892d8c11b477b9ce25ce71f17126cb2aaf5a4222202b7ae1dc2ae169fac6d39a6c360ef10f9de0f6500399ff0013fe1675e3c1061f49a668744cb8da2f2ed9e36008bdb95c8f55731b6ed512685e273d555cd533bb69ee2c1c8340b90d68e0d17dca5ca5f642e4bad4ff0010b6e247e5ac27a4b9dcbed3d1ac9d97d01557d49d69b2f31b2c1246dfc79af48b0d8cfe0b8d07da393293703c1ffb1e8b36a3fa8b5b3597d10c6df2bdf0bced6c3439afe36bd883cf78cd753d2fa9db2cc62c9e7da7f5577278d158efaf86d2888af5a222220d234cab84950c801ca3ef3bed38643c9bfea56ec192c669451cd0564b2b9a7b37bb6db25aedb1e04f023766a9c18a8e6b87eab5c97e45a6d0bae376c63888665159475ed2abb6a1a78aaa9acc361591790e0bd281f0ac4cd88be92a1b3b331ecc8ce0f6f11e3c41e6b2cb058fb81695b3c3c96c7962d5617ac5ab312df747f4ba82b328276978c9d138ec48d39e458ecf81cc5c2ce2e37d2860154e23886bbcf77e8abd0e9a62908b475b50072ed1ce1fcd75f42c76efa45bfaa8ad1a998760aa357571c4c2f95ed8da37b9ce0d03ccae50935918c91635f379168f980b5eafc4679ddb534b24aecec5ef73c8bf2b9c967a629e34e35d70441d161c04d2660cee0444cead1be43f2f1dca07c4f109aa2574d3c8e92471bb9ee3727f61c80c95b2f854891b5650599b5f13c9f2161fa152ed38ee8515680b83628fc2fea6ea50a6a81b21715d526673ccae78f1ac70ba4545d50158d5e2200deaba2b32f7d2fa5a80158cf8ab4715809aba496411c4d2f7bb20d6e64adb703d5f1367d63efc7b161b01f69e37f80f5565c6e9b93378878e4cf4a7960e3a99aa1fd9c0c73ddd370eae3b80f15bfe8a601f558ced10e95f62f70dc2db9ade82eb2d4547144c0c898d6347068b7ff4aaeba2e1f4ec7c7fc5e655d9b936c9e9ec2222b16b088883e10b0d88e8b51cd7da8834fc4cfe19f1cb23e616691637a56f1ab46d31698f0d12b357ee19c151e0d91b7fe66fecb0d55a358945fd58900e31bc3be4eb1f92951168e4e97c7bfb6becf7af2b247ba17762b244ed9958f8cf27b4b0fa396469b1a69e2a52a9a58e4696c8c6bda77b5cd0e1e8569d8c6ae29a4bba9deea77721df8ff09371e47c956e7e8513eb496cd39dfdd0c1cb898b6f5abe3f8a8b100dcf259e9f57389decd9a02df88b9ed3f8764fe6b62d14d5c454f209ea24ede569bb45b66361e61a7373ba9f4e2bcb8dd1af5beeccb2732baf462301d50d0c91326ae648f99ed05f1f6858c65ee4340658dc022f9efbad8a9356382c62c28a33f6cba43eae256de8ba6ad62b1110ad99dcedaa49ab7c18ff006187c811f9156353aa3c11ff00d9764f36cb2b7e41d65bca294229aed4461aece39aa633cb69920f42dbfcd6a98bea12adb9d3554520cfbb20744eb74236813e365d02883995987d4d04829ea59b1235a0ef0e0e6e6039ae191191f45b251e3396f52ce95e8a52d7c61b3348736fb12b6c1ecbf23c4743928d2bb54f5ec3fc09e295bc36b6a277a00e17f3551cbe9ff52db86f61e4f6c6a5465c5c5b7ab6a18a7ac984300b939977bac6f173cf00b2f856aaeb1c47d6678e36f111de471e9720007d549b806054f471767032c37b9c7373cf373b8fe4bc78fd2b56ddfc32c9ccf4d554346746e1a38ecc1b4f3edca4779c7f46f459a445775ac56351e1a133333b911116481111011110111101111011110111101111011110111101111011110111101111011110111101111011110111101111011110111101111011110111101111011110111107ffd9
\.


--
-- Data for Name: cuenta; Type: TABLE DATA; Schema: public; Owner: itb
--

COPY public.cuenta (id, id_cliente, saldo) FROM stdin;
\.


--
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: itb
--

COPY public.example (id, nombre) FROM stdin;
1 ejemplo
\.


--
-- Data for Name: movimientos; Type: TABLE DATA; Schema: public; Owner: itb
--

COPY public.movimientos (id, fecha, cantidad, id_origen, id_destino) FROM stdin;
\.


--
-- Name: cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itb
--

SELECT pg_catalog.setval('public.cliente_id_seq', 15, true);


--
-- Name: cuenta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itb
--

SELECT pg_catalog.setval('public.cuenta_id_seq', 1, false);


--
-- Name: example_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itb
--

SELECT pg_catalog.setval('public.example_id_seq', 1, true);


--
-- Name: movimientos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: itb
--

SELECT pg_catalog.setval('public.movimientos_id_seq', 1, false);


--
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- Name: cuenta cuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_pkey PRIMARY KEY (id);


--
-- Name: example example_pkey; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.example
    ADD CONSTRAINT example_pkey PRIMARY KEY (id);


--
-- Name: movimientos movimientos_pkey; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT movimientos_pkey PRIMARY KEY (id);


--
-- Name: cliente unico; Type: CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT unico UNIQUE (dni);


--
-- Name: cuenta id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT id_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- Name: movimientos id_origen; Type: FK CONSTRAINT; Schema: public; Owner: itb
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT id_origen FOREIGN KEY (id_origen) REFERENCES public.cuenta(id);


--
-- PostgreSQL database dump complete
--

