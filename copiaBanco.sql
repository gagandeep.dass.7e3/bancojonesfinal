--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-06-09 22:30:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 16569)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    nombre text,
    nacimiento date NOT NULL,
    apellidos character varying(200),
    sexo character(1),
    email character varying(200) NOT NULL,
    telefono character varying(9) NOT NULL,
    dni character varying(9) NOT NULL,
    password character varying(200) NOT NULL,
    image bytea
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16575)
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_seq OWNER TO postgres;

--
-- TOC entry 3029 (class 0 OID 0)
-- Dependencies: 201
-- Name: cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cliente_id_seq OWNED BY public.cliente.id;


--
-- TOC entry 202 (class 1259 OID 16577)
-- Name: cuenta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cuenta (
    id integer NOT NULL,
    id_cliente integer NOT NULL,
    saldo numeric(10,2)
);


ALTER TABLE public.cuenta OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16580)
-- Name: cuenta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cuenta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuenta_id_seq OWNER TO postgres;

--
-- TOC entry 3030 (class 0 OID 0)
-- Dependencies: 203
-- Name: cuenta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cuenta_id_seq OWNED BY public.cuenta.id;


--
-- TOC entry 204 (class 1259 OID 16582)
-- Name: example; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.example (
    id integer NOT NULL,
    nombre text
);


ALTER TABLE public.example OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16588)
-- Name: example_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.example_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.example_id_seq OWNER TO postgres;

--
-- TOC entry 3031 (class 0 OID 0)
-- Dependencies: 205
-- Name: example_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.example_id_seq OWNED BY public.example.id;


--
-- TOC entry 206 (class 1259 OID 16590)
-- Name: movimientos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movimientos (
    id integer NOT NULL,
    fecha timestamp without time zone,
    cantidad numeric(100,2),
    id_origen integer,
    id_destino integer
);


ALTER TABLE public.movimientos OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16593)
-- Name: movimientos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movimientos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimientos_id_seq OWNER TO postgres;

--
-- TOC entry 3032 (class 0 OID 0)
-- Dependencies: 207
-- Name: movimientos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movimientos_id_seq OWNED BY public.movimientos.id;


--
-- TOC entry 2870 (class 2604 OID 16619)
-- Name: cliente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente ALTER COLUMN id SET DEFAULT nextval('public.cliente_id_seq'::regclass);


--
-- TOC entry 2871 (class 2604 OID 16620)
-- Name: cuenta id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta ALTER COLUMN id SET DEFAULT nextval('public.cuenta_id_seq'::regclass);


--
-- TOC entry 2872 (class 2604 OID 16621)
-- Name: example id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.example ALTER COLUMN id SET DEFAULT nextval('public.example_id_seq'::regclass);


--
-- TOC entry 2873 (class 2604 OID 16622)
-- Name: movimientos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimientos ALTER COLUMN id SET DEFAULT nextval('public.movimientos_id_seq'::regclass);


--
-- TOC entry 3016 (class 0 OID 16569)
-- Dependencies: 200
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente (id, nombre, nacimiento, apellidos, sexo, email, telefono, dni, password, image) FROM stdin;
20	Gagandeep	2000-01-11	Dass Kaur	M	gagandeep.dass.7e3@itb.cat	632320543	54907150D	$2y$10$xjk6lGAHV4h7sv/zmRByg.s3.FH//wvfiCzcpAamDmZB1gSKb7pcK	\N
21	Arthur	1863-07-10	Morgan	M	email2@gmail.com	632236654	00000000T	$2y$10$rpFNCxajVt5KFJ0HQ.GWQeXtWZjAlSb0ezUsJ7QEdo5saUk/QeXeW	\N
23	Arthur	1863-07-10	Morgan	M	email2@gmail.com	632236654	90772520F	$2y$10$0rlusG3qJb8mnIjoN6M8QODt072FivlYaA.PncwuZVxJFIe4ibpzO	\N
\.


--
-- TOC entry 3018 (class 0 OID 16577)
-- Dependencies: 202
-- Data for Name: cuenta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cuenta (id, id_cliente, saldo) FROM stdin;
\.


--
-- TOC entry 3020 (class 0 OID 16582)
-- Dependencies: 204
-- Data for Name: example; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.example (id, nombre) FROM stdin;
\.


--
-- TOC entry 3022 (class 0 OID 16590)
-- Dependencies: 206
-- Data for Name: movimientos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movimientos (id, fecha, cantidad, id_origen, id_destino) FROM stdin;
\.


--
-- TOC entry 3033 (class 0 OID 0)
-- Dependencies: 201
-- Name: cliente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cliente_id_seq', 23, true);


--
-- TOC entry 3034 (class 0 OID 0)
-- Dependencies: 203
-- Name: cuenta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cuenta_id_seq', 1, false);


--
-- TOC entry 3035 (class 0 OID 0)
-- Dependencies: 205
-- Name: example_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.example_id_seq', 1, true);


--
-- TOC entry 3036 (class 0 OID 0)
-- Dependencies: 207
-- Name: movimientos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movimientos_id_seq', 1, false);


--
-- TOC entry 2875 (class 2606 OID 16600)
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 2879 (class 2606 OID 16602)
-- Name: cuenta cuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_pkey PRIMARY KEY (id);


--
-- TOC entry 2881 (class 2606 OID 16604)
-- Name: example example_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.example
    ADD CONSTRAINT example_pkey PRIMARY KEY (id);


--
-- TOC entry 2883 (class 2606 OID 16606)
-- Name: movimientos movimientos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT movimientos_pkey PRIMARY KEY (id);


--
-- TOC entry 2877 (class 2606 OID 16608)
-- Name: cliente unico; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT unico UNIQUE (dni);


--
-- TOC entry 2884 (class 2606 OID 16609)
-- Name: cuenta id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT id_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- TOC entry 2885 (class 2606 OID 16614)
-- Name: movimientos id_origen; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movimientos
    ADD CONSTRAINT id_origen FOREIGN KEY (id_origen) REFERENCES public.cuenta(id);


-- Completed on 2021-06-09 22:30:09

--
-- PostgreSQL database dump complete
--

