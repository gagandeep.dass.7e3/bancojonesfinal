# Catalan translations for PACKAGE package.
# Copyright (C) 2020 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# itb <root@itb.itb>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-25 17:14+0100\n"
"PO-Revision-Date: 2020-11-25 17:17+0100\n"
"Last-Translator: itb <root@itb.itb>\n"
"Language-Team: Catalan\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"

#: View/register.php:10
msgid "Registro"
msgstr "Registre"
